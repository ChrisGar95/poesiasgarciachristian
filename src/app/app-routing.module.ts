import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./shared/pages/login/login.module').then( m => m.LoginPageModule)
  },
  
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  
  
  {
    path: 'crear-poesia',
    loadChildren: () => import('./shared/pages/crear-poesia/crear-poesia.module').then( m => m.CrearPoesiaPageModule)
  },
  {
    path: 'lista-poesias',
    loadChildren: () => import('./shared/pages/lista-poesias/lista-poesias.module').then( m => m.ListaPoesiasPageModule)
  },
  {
    path: 'poesia/:id',
    loadChildren: () => import('./shared/pages/poesia/poesia.module').then( m => m.PoesiaPageModule)
  },


  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
