import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AuthService } from '../../services/auth.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public router: Router,private googlePlus: GooglePlus,private auth: AuthService,public alertController: AlertController) { }

  ngOnInit() {
  }

  async loginGoogle(){
    const error = await this.auth.googleLogin();
    if(error==undefined){
      this.router.navigate(['lista-poesias']);
    
    }
    else{
      //alert(JSON.stringify(error));
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Error de Autentificacion',
        //subHeader: 'Subtitle',
        message: 'Intente de nuevo',
        buttons: ['OK']
      });
      await alert.present();
    }
  }

}
