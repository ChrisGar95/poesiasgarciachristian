import { Component, OnInit } from '@angular/core';
import { PoesiasService } from '../../services/poesias.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Poesia } from '../../model/poesia';
import { Camera } from '@ionic-native/camera/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-poesia',
  templateUrl: './poesia.page.html',
  styleUrls: ['./poesia.page.scss'],
})
export class PoesiaPage implements OnInit {

  /*Cuando quiero utilizar con suscribe
  poesia: Observable<any>
  poesiaUpdate: poesia = new poesia()*/
  
  //Cuando utilizo con el await
  poesia: Poesia = new Poesia()
 
  //icono:string='camera';
  comentarioPoesia: string;

  constructor(public router: Router, private poesiasService: PoesiasService, public alertController: AlertController, private route: ActivatedRoute, private camera: Camera) { }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id")
    
    /*------------Una forma de obtener un objeto manipulable--------//
    this.poesia = this.poesiasService.getpoesia(id)
    this.poesia.subscribe(data => {this.poesiaUpdate=data;})//
    //-------------------------------------------------------------*/

    //------------Otra Forma de obtener un objeto manipulable-----//
    this.poesia = await this.poesiasService.getPoesiaById(id);
    
    //-----------------------------------------------------------//
    
  }

  actualizaPoesia()
  {
    /*Cuando utilizo con el suscribe
    this.poesiasService.savepoesia(this.poesiaUpdate);*/

    this.poesia.comentario=this.poesia.comentario+", "+this.comentarioPoesia;
    this.poesiasService.savePoesia(this.poesia);

    this.comentarioPoesia="";
    
    this.router.navigate(['poesia']);
    
  }

  imagenCargada(e){
    this.poesia.image = e;
    this.presentAlert();
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Hecho',
      message: 'Imagen Subida',
      buttons: ['OK']
    });

    await alert.present();
  }

}
