import { Component, OnInit } from '@angular/core';
import { Poesia } from '../../model/poesia';
import { PoesiasService } from '../../services/poesias.service';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-crear-poesia',
  templateUrl: './crear-poesia.page.html',
  styleUrls: ['./crear-poesia.page.scss'],
})
export class CrearPoesiaPage implements OnInit {

  poesia: Poesia = new Poesia()
  base64Image: any;

  icono:string='camera';

  fechaActual: string

  constructor(public router: Router,private poesiasService: PoesiasService,private camera: Camera, public alertController: AlertController) { }

  ngOnInit() {
    this.fechaActual = new Date().toISOString();
  }

  saludar(data){
    console.log(data);
  }

  imagenCargada(e){
    this.poesia.image = e;
    this.presentAlert();
  }

  guardarPoesia(e){
    //console.log(this.poesia)
    this.poesia.fecha=this.fechaActual.substring(0,10);
    this.poesiasService.savePoesia(this.poesia);
    this.router.navigate(['lista-poesias']);
    
  }

  tomarFoto()
  {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.base64Image = 'data:image/jpeg;base64,' + imageData;
     console.log(this.base64Image);
    }, (err) => {
     // Handle error
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Hecho',
      message: 'Imagen Subida',
      buttons: ['OK']
    });

    await alert.present();
  }

  

}

