import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PoesiasService } from '../../services/poesias.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-poesias',
  templateUrl: './lista-poesias.page.html',
  styleUrls: ['./lista-poesias.page.scss'],
})
export class ListaPoesiasPage implements OnInit {

  poesias: Observable<any[]>;

  constructor(private poesiasService: PoesiasService, public router: Router) { }

  ngOnInit() {

    this.poesias= this.poesiasService.getPoesias();
    
  }

  showPoesia(id: any)
  {
    this.router.navigate([`poesia/${id}`])
    
  }

  //se utiliza cuando tengo una lista observable o asincrona
  trackByFn(index, obj) {
    return obj.uid;
  }

  showCrearPoesia()
  {
    this.router.navigate(['crear-poesia'])
  }


}

