import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, throwError } from 'rxjs';
import { Poesia } from '../model/poesia';
import { first, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PoesiasService {

  constructor(private afs: AngularFirestore) { }

  getPoesias(): Observable<any[]>{
    return this.afs.collection('poesias',ref => ref.orderBy("fecha",'asc')).valueChanges();
       
  }

  //Devuelve un empleado Observable
  getPoesia(uid: string): Observable<any>{
    let itemDoc = this.afs.doc<any>(`poesias/${uid}`);
    return itemDoc.valueChanges();
  }

  //Guarda o Actualiza
  savePoesia(poesia: Poesia){
    const refpoesia = this.afs.collection("poesias")
    if(poesia.uid==null){
      poesia.uid= this.afs.createId()
    }
    const param = JSON.parse(JSON.stringify(poesia));
    refpoesia.doc(poesia.uid).set(param, {merge: true})

  }

  //Devuelve un empleado manipulable
  async getPoesiaById(uid: string): Promise<Poesia>
  {
    try{
        let aux:any = await this.afs.collection("poesias", 
            ref => ref.where('uid', '==', uid))
                      .valueChanges().pipe(first()).toPromise().then(doc => {                    	  
                          return doc;
                      }).catch(error => {
                          throw error;
                      });
        if(aux.length==0)
            return undefined;
        return aux[0];
    }catch(error){
      console.error("Error", error);
      throw error;
    } 
  }
  
}

