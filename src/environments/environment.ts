// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCDZhgkBEsafaQW7Bm4GzxlDbhG3h2RBaQ",
    authDomain: "poesiasgarciachristian.firebaseapp.com",
    databaseURL: "https://poesiasgarciachristian.firebaseio.com",
    projectId: "poesiasgarciachristian",
    storageBucket: "poesiasgarciachristian.appspot.com",
    messagingSenderId: "33680530649",
    appId: "1:33680530649:web:b9fa0eb9a0fe399edff2c8"
  },
  googleWebClientId: '33680530649-rbqgecmp3m6l2mcjpvfclkcm250debn9.apps.googleusercontent.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
